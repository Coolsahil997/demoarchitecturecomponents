package com.example.sahil.githubissuesdemo.database;

import android.content.Context;
import android.util.Log;

import com.example.sahil.githubissuesdemo.interfaces.MyWebService;
import com.example.sahil.githubissuesdemo.observable.MyObservale;
import com.example.sahil.githubissuesdemo.responseModel.CommentsModel;
import com.example.sahil.githubissuesdemo.responseModel.IssuesModel;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import androidx.lifecycle.LiveData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AppRepository {
    private static AppRepository ourInstance;
    public ArrayList<IssuesModel> mIssuesModelArrayList = new ArrayList<>();
    public ArrayList<IssuesEntity> mIssuesEntityArrayList;
    public ArrayList<CommentsModel> commentsModels;
    private Executor mExecutor = Executors.newSingleThreadExecutor();
    private AppDatabase mDatabase;
    public ArrayList<IssuesEntity> issuesEntities = new ArrayList<>();

    public static AppRepository getInstance(Context context) {
        return ourInstance = new AppRepository(context);
    }

    private AppRepository(Context context) {
        mDatabase = AppDatabase.getInstance(context);

    }

    public List<IssuesEntity> getIssuesFromDb() {

        return mDatabase.issuesDao().getAllIssues();


    }

    public void getIssuesFromGithub() {

        MyWebService webService = MyWebService.retrofit.create(MyWebService.class);

        Call<ArrayList<IssuesEntity>> call = webService.getIssues();

        call.enqueue(new Callback<ArrayList<IssuesEntity>>() {
            @Override
            public void onResponse(Call<ArrayList<IssuesEntity>> call, final Response<ArrayList<IssuesEntity>> response) {
                if (response.isSuccessful()) {
                    if (response.isSuccessful()) {
                        mIssuesEntityArrayList = response.body();
                        mExecutor.execute(new Runnable() {
                            @Override
                            public void run() {
                                mDatabase.issuesDao().insertAllIssue(mIssuesEntityArrayList);
                                MyObservale.getInstance().setData(mIssuesEntityArrayList, 1);
                            }
                        });


                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<IssuesEntity>> call, Throwable t) {
                Log.e("failure", t.getMessage());

            }
        });


    }

    public void getComments(int id) {
        MyWebService webService = MyWebService.retrofit.create(MyWebService.class);

        Call<ArrayList<CommentsModel>> call = webService.getComments(id);
        call.enqueue(new Callback<ArrayList<CommentsModel>>() {
            @Override
            public void onResponse(Call<ArrayList<CommentsModel>> call, Response<ArrayList<CommentsModel>> response) {
                commentsModels = response.body();
                MyObservale.getInstance().setData(commentsModels, 2);
            }

            @Override
            public void onFailure(Call<ArrayList<CommentsModel>> call, Throwable throwable) {

            }
        });


    }
}
