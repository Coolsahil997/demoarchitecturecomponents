package com.example.sahil.githubissuesdemo.interfaces;

import com.example.sahil.githubissuesdemo.database.IssuesEntity;
import com.example.sahil.githubissuesdemo.responseModel.CommentsModel;

import org.w3c.dom.Comment;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface MyWebService {
    String BASE_URL="https://api.github.com/repos/firebase/firebase-ios-sdk/";
    String FEED="issues";

    Retrofit retrofit=new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();


    @GET(FEED)
    Call<ArrayList<IssuesEntity>> getIssues();

    @GET("issues/{id}/comments")
    Call<ArrayList<CommentsModel>>getComments(@Path("id") int groupId);
}
