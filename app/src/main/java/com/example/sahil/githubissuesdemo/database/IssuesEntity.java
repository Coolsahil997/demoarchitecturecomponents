package com.example.sahil.githubissuesdemo.database;

import java.util.Date;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "issues")
public class IssuesEntity {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String title;
    private Date date;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    private int number;

    public String getComments_url() {
        return comments_url;
    }

    public void setComments_url(String comments_url) {
        this.comments_url = comments_url;
    }

    private String comments_url;

    public IssuesEntity(int id, String title, Date date, String comments_url, int number) {
        this.id = id;
        this.title = title;
        this.date = date;
        this.comments_url = comments_url;
        this.number = number;
    }

    @Ignore
    public IssuesEntity(String title, Date date, String comments_url, int number) {
        this.comments_url = comments_url;
        this.title = title;
        this.date = date;
        this.number = number;
    }

    @Ignore
    public IssuesEntity() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
