package com.example.sahil.githubissuesdemo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.sahil.githubissuesdemo.R;
import com.example.sahil.githubissuesdemo.activity.IssuesActivity;
import com.example.sahil.githubissuesdemo.database.IssuesEntity;
import com.example.sahil.githubissuesdemo.interfaces.IRecyclerViewClickListener;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class IssuesAdapter extends RecyclerView.Adapter<IssuesAdapter.MyViewHolder> {

    private ArrayList<IssuesEntity> items = new ArrayList<>();
    Context context;
    IRecyclerViewClickListener iRecyclerViewClickListener;
    View view;

    public IssuesAdapter(Context context, ArrayList<IssuesEntity> items, IRecyclerViewClickListener iRecyclerViewClickListener) {
        this.items = items;
        this.context = context;
        this.iRecyclerViewClickListener = iRecyclerViewClickListener;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.issue_adpter_layout, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int i) {

        myViewHolder.tv_title.setText(items.get(i).getTitle());
        myViewHolder.tv_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iRecyclerViewClickListener.onRecyclerClick(v, i);
            }
        });

        // mainActivity.setClickListnere(myViewHolder.tvTitle, items, i);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public final TextView tv_title;


        public MyViewHolder(@NonNull final View itemView) {
            super(itemView);
            tv_title = itemView.findViewById(R.id.tv_title);


        }
    }
}