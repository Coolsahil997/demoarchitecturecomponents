package com.example.sahil.githubissuesdemo.viewmodel;

import android.app.Application;

import com.example.sahil.githubissuesdemo.database.AppRepository;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

public class CommentsViewModel extends AndroidViewModel {
    private AppRepository mAppRepository;
    public CommentsViewModel(@NonNull Application application) {
        super(application);
        mAppRepository = AppRepository.getInstance(application.getApplicationContext());
    }

    public void getComments(int comments_id) {
        mAppRepository.getComments(comments_id);

    }
}
