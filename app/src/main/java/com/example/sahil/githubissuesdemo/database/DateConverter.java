package com.example.sahil.githubissuesdemo.database;

import java.util.Date;

import androidx.room.TypeConverter;

public class DateConverter {
    @TypeConverter
    public static Long toTimeStamp(Date date)
    {
        return date==null?null:date.getTime();
    }

    @TypeConverter
    public static Date toDate(Long timeStamp)
    {
        return timeStamp==null?null:new Date(timeStamp);
    }
}
