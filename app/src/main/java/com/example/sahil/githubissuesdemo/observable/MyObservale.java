package com.example.sahil.githubissuesdemo.observable;

import java.util.Observable;

public class MyObservale<T> extends Observable {

    private MyObservale(){}
    private static MyObservale instance;

    private T data;

    public static MyObservale getInstance(){
        if(instance==null)
            instance=new MyObservale();
        return instance;
    }


    public T getData(){
        return data;
    }

    public void setData(T data,int value){
        if(data!=null){
            this.data =data;

            setChanged();
            notifyObservers(value);
        }
    }

}
