package com.example.sahil.githubissuesdemo.viewmodel;

import android.app.Application;

import com.example.sahil.githubissuesdemo.database.AppRepository;
import com.example.sahil.githubissuesdemo.database.IssuesEntity;
import com.example.sahil.githubissuesdemo.responseModel.IssuesModel;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

public class IssuesViewModel extends AndroidViewModel {

    public ArrayList<IssuesModel> mIssuesModelArrayList = new ArrayList<>();
    public LiveData<ArrayList<IssuesEntity>> mIssuesEntityArrayList=new LiveData<ArrayList<IssuesEntity>>() {
    } ;
    private AppRepository mAppRepository;

    public IssuesViewModel(@NonNull Application application) {
        super(application);
        mAppRepository = AppRepository.getInstance(application.getApplicationContext());
    }

    public void getIssuesFromGithub() {
        mAppRepository.getIssuesFromGithub();


    }
    public List<IssuesEntity> getAllIssues()
    {
        return mAppRepository.getIssuesFromDb();
    }
}
