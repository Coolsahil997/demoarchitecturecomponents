package com.example.sahil.githubissuesdemo.activity;


import android.content.Intent;
import android.os.Bundle;

import com.example.sahil.githubissuesdemo.R;

import androidx.appcompat.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent i = new Intent(SplashActivity.this, IssuesActivity.class);
        startActivity(i);
    }
}
