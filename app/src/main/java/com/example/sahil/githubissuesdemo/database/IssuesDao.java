package com.example.sahil.githubissuesdemo.database;

import java.util.ArrayList;
import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
public interface IssuesDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertIssue(IssuesEntity issuesEntity);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllIssue(ArrayList<IssuesEntity> issuesEntity);

    @Delete
    void deleteIssue(IssuesEntity issuesEntity);


    @Query("Select * From issues ORDER BY date desc")
    List<IssuesEntity> getAllIssues();

    @Query("Delete from issues")
    void deleteAllIssues();
}
