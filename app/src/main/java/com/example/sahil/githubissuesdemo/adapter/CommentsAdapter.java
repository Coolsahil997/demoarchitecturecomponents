package com.example.sahil.githubissuesdemo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.sahil.githubissuesdemo.R;
import com.example.sahil.githubissuesdemo.database.IssuesEntity;
import com.example.sahil.githubissuesdemo.interfaces.IRecyclerViewClickListener;
import com.example.sahil.githubissuesdemo.responseModel.CommentsModel;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.MyViewHolder> {

    private ArrayList<CommentsModel> items = new ArrayList<>();
    Context context;
    IRecyclerViewClickListener iRecyclerViewClickListener;
    View view;

    public CommentsAdapter(Context context, ArrayList<CommentsModel> items, IRecyclerViewClickListener iRecyclerViewClickListener) {
        this.items = items;
        this.context = context;
        this.iRecyclerViewClickListener = iRecyclerViewClickListener;
    }


    @NonNull
    @Override
    public CommentsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.issue_adpter_layout, parent, false);
        CommentsAdapter.MyViewHolder myViewHolder = new CommentsAdapter.MyViewHolder(view);
        return myViewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull CommentsAdapter.MyViewHolder myViewHolder, final int i) {

        myViewHolder.tv_title.setText(items.get(i).getBody());
        myViewHolder.tv_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iRecyclerViewClickListener.onRecyclerClick(v, i);
            }
        });

        // mainActivity.setClickListnere(myViewHolder.tvTitle, items, i);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public final TextView tv_title;


        public MyViewHolder(@NonNull final View itemView) {
            super(itemView);
            tv_title = itemView.findViewById(R.id.tv_title);


        }
    }
}