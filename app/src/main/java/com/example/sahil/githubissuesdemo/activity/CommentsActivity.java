package com.example.sahil.githubissuesdemo.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.sahil.githubissuesdemo.R;
import com.example.sahil.githubissuesdemo.adapter.CommentsAdapter;
import com.example.sahil.githubissuesdemo.adapter.IssuesAdapter;
import com.example.sahil.githubissuesdemo.database.IssuesEntity;
import com.example.sahil.githubissuesdemo.interfaces.IRecyclerViewClickListener;
import com.example.sahil.githubissuesdemo.observable.MyObservale;
import com.example.sahil.githubissuesdemo.responseModel.CommentsModel;
import com.example.sahil.githubissuesdemo.viewmodel.CommentsViewModel;
import com.example.sahil.githubissuesdemo.viewmodel.IssuesViewModel;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class CommentsActivity extends AppCompatActivity implements IRecyclerViewClickListener, Observer {
    private int comments_id;
    private RecyclerView rv_comments;
    private CommentsViewModel commentsViewModel;
    private IRecyclerViewClickListener iRecyclerViewClickListener;
    private ArrayList<CommentsModel> commentsModelsArrayList = new ArrayList<>();
    private CommentsAdapter commentsAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);
        comments_id = getIntent().getIntExtra("comments", 0);
        initView();
    }

    private void initView() {
        iRecyclerViewClickListener = this;
        MyObservale.getInstance().addObserver(CommentsActivity.this);
        commentsViewModel = ViewModelProviders.of(this).get(CommentsViewModel.class);
        rv_comments = findViewById(R.id.rv_comments);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rv_comments.setLayoutManager(linearLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rv_comments.getContext(),
                linearLayoutManager.getOrientation());
        rv_comments.addItemDecoration(dividerItemDecoration);
        commentsViewModel.getComments(comments_id);
    }

    @Override
    public void onRecyclerClick(View view, int position) {

    }

    @Override
    public void update(Observable o, Object arg) {
        MyObservale myObservale = (MyObservale) o;
        try {
            Log.i("savedCards", o.toString());
            if ((int) arg == 2) {
                commentsModelsArrayList = (ArrayList<CommentsModel>) (myObservale).getData();
                if (commentsModelsArrayList.size() > 0) {
                    showData();
                } else {
                    Toast.makeText(this, "No Comments", Toast.LENGTH_LONG).show();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void showData() {
        commentsAdapter = new CommentsAdapter(CommentsActivity.this, commentsModelsArrayList, iRecyclerViewClickListener);
        rv_comments.setAdapter(commentsAdapter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        /**
         * destroying observable
         */
        MyObservale.getInstance().deleteObserver(this);
    }

}
