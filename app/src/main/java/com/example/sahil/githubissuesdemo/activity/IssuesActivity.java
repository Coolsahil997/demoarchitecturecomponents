package com.example.sahil.githubissuesdemo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;


import com.example.sahil.githubissuesdemo.R;
import com.example.sahil.githubissuesdemo.adapter.IssuesAdapter;
import com.example.sahil.githubissuesdemo.database.IssuesEntity;
import com.example.sahil.githubissuesdemo.interfaces.IRecyclerViewClickListener;
import com.example.sahil.githubissuesdemo.observable.MyObservale;
import com.example.sahil.githubissuesdemo.responseModel.IssuesModel;
import com.example.sahil.githubissuesdemo.viewmodel.IssuesViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


public class IssuesActivity extends AppCompatActivity implements java.util.Observer, IRecyclerViewClickListener {
    private RecyclerView rv_issues;
    private IssuesViewModel mIssuesViewModel;
    private List<IssuesEntity> mIssueModelArrayList = new ArrayList<>();
    private ArrayList<IssuesEntity> mIssueEntityArrayList = new ArrayList<>();
    private IssuesAdapter issuesAdapter;
    private Observer<List<IssuesEntity>> listObserver;
    private IRecyclerViewClickListener iRecyclerViewClickListener;
    private Executor mExecutor = Executors.newSingleThreadExecutor();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_issues);
        initView();
        getIssues();
    }

    private void initView() {
        iRecyclerViewClickListener = this;
        MyObservale.getInstance().addObserver(IssuesActivity.this);
        mIssuesViewModel = ViewModelProviders.of(this).get(IssuesViewModel.class);
        rv_issues = findViewById(R.id.rv_issues);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rv_issues.setLayoutManager(linearLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rv_issues.getContext(),
                linearLayoutManager.getOrientation());
        rv_issues.addItemDecoration(dividerItemDecoration);
    }

    private void getIssues() {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {
                mIssueEntityArrayList = (ArrayList<IssuesEntity>) mIssuesViewModel.getAllIssues();
                if(mIssueEntityArrayList.size()>0)
                {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showData();
                        }
                    });
                }
                else
                {
                    mIssuesViewModel.getIssuesFromGithub();
                }
            }
        });
    }

    private void showData() {
        issuesAdapter = new IssuesAdapter(IssuesActivity.this, mIssueEntityArrayList, iRecyclerViewClickListener);
        rv_issues.setAdapter(issuesAdapter);
    }


    @Override
    public void update(Observable o, Object arg) {
        MyObservale myObservale = (MyObservale) o;
        try {
            Log.i("savedCards", o.toString());
            if ((int) arg == 1) {
                mExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        mIssueEntityArrayList = (ArrayList<IssuesEntity>) mIssuesViewModel.getAllIssues();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                showData();
                            }
                        });

                    }
                });

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        /**
         * destroying observable
         */
        MyObservale.getInstance().deleteObserver(this);
    }

    @Override
    public void onRecyclerClick(View view, int position) {
        int comment_id = mIssueEntityArrayList.get(position).getNumber();
        Intent i = new Intent(IssuesActivity.this, CommentsActivity.class);
        i.putExtra("comments", comment_id);
        startActivity(i);
    }
}
